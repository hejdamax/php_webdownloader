<?php

// header("Content-Type: application/vnd.ms-word");
// header("Expires: 0");
// header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
// header("content-disposition: attachment;filename=Report.doc");

require("vendor/autoload.php");
require_once("crawler.php");
use voku\helper\HtmlDomParser;

$linkArray = array();

$parseLinksObject = new ParseLinks('https://orlaneinstitut.cz/', 5000);
$linkArray = $parseLinksObject->getAllLinks();

foreach($linkArray as $key => $one) 
{
    if(strpos($one, 'jpg') !== false || strpos($one, 'jpeg') !== false || strpos($one, 'png') !== false || strpos($one, 'svg') !== false )
    {
       unset($linkArray[$key]);
    }

    $file_headers = @get_headers($one);
    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found')
    {
        unset($linkArray[$key]);
    }
}
//print_r($linkArray);
foreach($linkArray as $L)
{
    
    
    $doc = new DOMDocument();
    $doc->formatOutput = true;

    $url = parse_url($L);
    $name = $url['host'].str_replace("/", "_", $url['path']);
    $nameHTML = "html/".$name.".html";
    $nameXML = "xml/".$name.".xml";

    @$doc->loadHTML(file_get_contents($L));

    $dom = HtmlDomParser::file_get_html($L);

    // foreach ($dom->find('img') as $e) 
    // {
    //     $e->outertext = '';
    // }
    foreach ($dom->find('head') as $e) 
    {
        $e->outertext = '';
    }
    foreach ($dom->find('svg') as $e) 
    {
        $e->outertext = '';
    }
    foreach ($dom->find('script') as $e) 
    {
        $e->outertext = '';
    }
    foreach ($dom->find('comment') as $e) 
    {
        $e->outertext = '';
    }
    foreach ($dom->find('a') as $e) 
    {
        if(strpos($e -> href, "instagram"))
        {
            $e->outertext = '';
        }   
    }
    foreach ($dom->find('*[class]') as $e) 
    {
        $e->removeAttribute('class');
    }
    $document = $dom->save();

    $tidy = new tidy();
    $config = array('indent' => TRUE,
                'output-xml' => TRUE,
                'hide-comments' => TRUE,
                'wrap' => 400
                    );   
    $document = $tidy -> repairString($document, $config);
    $document = str_replace("&nbsp;", " ", $document);

    file_put_contents($nameHTML,$doc->saveHTML());
    file_put_contents($nameXML,$document);
    $counter++;
}
echo "<div style='width:50%;float:left;'>";
echo "<h2>HTML FILES</h2>";
foreach($linkArray as $L)
{
    $url = parse_url($L);
    $name = $url['host'].str_replace("/", "_", $url['path']);
    $nameHTML = "html/".$name.".html";
    echo "<a href=".$nameHTML.">".$L."</a>";
    echo "<br>";
}
echo "</div>";
echo "<div style='width:50%;float:right;'>";
echo "<h2>XML FILES</h2>";
foreach($linkArray as $L)
{
    $url = parse_url($L);
    $name = $url['host'].str_replace("/", "_", $url['path']);
    $nameXML = "xml/".$name.".xml";
    echo "<a href=".$nameXML.">".$L."</a>";
    echo "<br>";
}
echo "</div>";
?>