# Web Downloader

This application can crawl a website and download the pages in the html and xml format.

## Installation

1. Install apache web server. For convience you can use [XAMPP](https://www.apachefriends.org/index.html). For download and installation of *XAMPP* please refer to this [page](https://www.apachefriends.org/download.html). You only need *apache* and *php*.
2. Downolad this git repo and unpack it into the **htdocs** directory in the *XAMPP* installation location.

## Running

If you are running this app multiple times it is good to clean up the xml and html directories (these aren't cleaned up automatically).

1. Start *XAMPP* based on your system (refer to [XAMPP FAQ](https://www.apachefriends.org/download.html), FAQ section is in right hand panel).
2. Edit this line: `$parseLinksObject = new ParseLinks('https://orlaneinstitut.cz/', 5000);` in index.php (located in `XAMPP_INSTALL_DIR/htdocs/` - there might be one extra directory called php_webdownloader-master if you just unpacked the git repository). Only change the url and number part of this line to the url of the website that you want to download (the url has to start with `http:// or https://`). The number means the maximum number of pages that will be downloaded. Change this if your website is bigger than 5000 pages or if you only want a part of the website.
3. Go to `http://localhost/` - after the slash goes the path to index.php from the last point.

Don't be scared if the application isn't super quick. Crawling and postprocessing takes some time.

## Viewing the output files

After the application finishes it will display a list of the xml and html files with links for quick viewing. The files are physically located in the same directory as the index.php in directories called html and xml.

For example `XAMPP_INSTALL_DIR/htdocs/php_webdownloader_master/xml` would lead to the xml directory if you just unpacked the archive into htdocs.

## Extra infromation

If you want to modify how the output xml files are generated you can edit `index.php` and uncomment this

``` php
foreach ($dom->find('img') as $e)
{
     $e->outertext = '';
}
```

to disable image print in xml output. By commenting out these codeblocks you can turn on output of things like svg images, head tag of page comments or classes into xml. These things aren't outputted by default because the xml files are meant to be used for things where it is useful to have the output as close to plaintext as possible.
